# XRandR-Parser

XRandR-Parser is a interface for parsing the output of `xrandr --query` into
Rust Stuctures and filter through methods.

# Usage

```rust
#[allow(non_snake_case)]

use xrandr_parser::Parser;

fn main() -> Result<(), String> {
    let mut XrandrParser = Parser::new();

    XrandrParser.parse()?;

    let connector = &XrandrParser.get_connector("HDMI-1")?;

    let available_resolutions = &connector.available_resolutions_pretty()?;
    let available_refresh_rates = &connector.available_refresh_rates("1920x1080")?;

    println!(
        "Available Resolutions for HDMI-1: {:#?}",
        available_resolutions
    );
    println!(
        "Available Refresh Rates for HDMI-1 @ 1920x1080: {:#?}",
        available_refresh_rates
    );
    Ok(())
}
```

_Using: examples/example_output_

Would Output:

```
Available Resolutions for HDMI-1: [
    "1920x1080",
]
Available Refresh Rates for HDMI-1 @ 1920x1080: [
    "60.00",
]
```

# Installation

Add the following line to your Cargo.toml file:

```toml
xrandr-parser = "0.3.0"
```

Or use the development version:

```toml
xrandr-parser = { git = "https://gitlab.com/Th3-S1lenc3/xrandr-parser" }
```

_Warning: The development version is not guaranteed to be stable. Use at your own discretion._

# Testing

```sh
$ EXAMPLE_DIR=$(pwd)/examples cargo test --doc --features "test"
```

# License

All files in this project are distributed under the GNU General Public License v3.0 or later.
